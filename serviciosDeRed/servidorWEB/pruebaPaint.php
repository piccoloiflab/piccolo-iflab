#!/usr/bin/php
<html>
<head><title>Paint your Dreams</title>

<center>
<canvas id="canvas" width="650" height="450" 
style="border:5px solid black; background-color:white;">
</canvas>
</center>

<!--Entradas -->
<center>
<form><left>
<input type="button" value="Pencil" onclick="pencil()" >
<input type="button" value="Line" onclick="line()" >
<input type="button" value="Rect" onclick="rectangle()" >
<input type="button" value="Circle" onclick="circle()" >
<input type="button" value="Brush" onclick="brush()" >
<input type="button" value="Spray" onclick="spray()" >
<input type="button" value="Erase" onclick="erase()" >
<input type="button" value="Reset" onclick="resete()" >
<input type="radio" name="fille" onclick="fill()" value="1" >Fill
<input type="radio" name="fille" onclick="border()" value="0">Border
<input type="button" value="Save" onclick="save()" ><br>
<input type="button" value="Pixel+" onclick="pixelplus()" >
<input type="button" value="Pixel-" onclick="pixelminus()" >
<input type="button" value="PixelNorm" onclick="pixelnormal()" >

</left></form>
</center>

<table><center>
<table border=2 bgcolor=#FFFFFF align="left">

<td><div onclick = "color('#0000ff')" 
style ="width:20px;height:20px;background-color:#0000ff"></div><td>

<td><div onclick = "color('#000000')" 
style ="width:20px;height:20px;background-color:#000000"></div><td>

</tr>
</table></center>

<script>

//set background to an image

document.body.background="sis.png";

//Canvas initialization

var canvas = document.getElementById("canvas");
var context=canvas.getContext('2d');
context.lineWidth=1;

//Function to make pixel size normal

function pixelnormal(){
context.lineWidth=1;
}

//Function to increase pixel size

function pixelplus(){
context.lineWidth=context.lineWidth+1;
}

//Function to decrease pixel size

function pixelminus(){
if(context.lineWidth>1){
context.lineWidth=context.lineWidth-1;
}
}

//Fill checking

function fill(){
f=1;
} 

//Border checking

function border(){
f=0;
}


//Save option

function save() {
  var imgg=canvas.toDataURL("image/png");
  window.location = imgg;
}


//Reset tool

function resete(){
context.clearRect(0,0,650,450);
}

//Pencil tool

function pencil(){
canvas.onmousedown=pencildown;
canvas.onmouseup=pencilup;
canvas.onmousemove=pencilmove;
function pencildown(e){
x=e.x;
y=e.y;
draw=true;
}
function pencilup(){
draw=false;
}
function pencilmove(event){
if (draw){
   a =event.x;
   b =event.y;
   context.beginPath();
   context.moveTo(x,y);
   context.lineTo(a,b);
   context.stroke();
   context.closePath();
   x=a;
   y=b;
 }}}


//Line tool

function line(){
canvas.onmousedown=linedown;
canvas.onmouseup=lineup;
canvas.onmousemove=linemove;
function linedown(e){
 img=context.getImageData(0,0,canvas.width,canvas.height);
 startx=e.x;
 starty=e.y;
 drag1=true;
}
function lineup(e){
 drag1=false;
}
function linemove(e){
 if (drag1){
 context.putImageData(img,0,0);
 endx=e.x;
 endy=e.y;
 context.beginPath();
 context.moveTo(startx,starty);
 context.lineTo(endx,endy);
 context.stroke();
 context.closePath();
}}}

//Rectangle tool

function rectangle(){
canvas.onmousedown=rectdown;
canvas.onmouseup=rectup;
canvas.onmousemove=rectmove;
function rectdown(e){
 img=context.getImageData(0,0,canvas.width,canvas.height);
 startx=e.x;
 starty=e.y;
 drag2=true;
}
function rectup(e){
drag2=false;
}
function rectmove(e){
if (drag2){
context.putImageData(img,0,0);
rectw=e.x-startx;
recth=e.y-starty;
context.strokeRect(startx,starty,rectw,recth);
if (f==1){
context.fillRect(startx,starty,rectw,recth);
}
}}}

//Circle tool

function circle(){
canvas.onmousedown=circledown;
canvas.onmouseup=circleup;
canvas.onmousemove=circlemove;
function circledown(e){
 img=context.getImageData(0,0,canvas.width,canvas.height);
 startx=e.x;
 starty=e.y;
 drag3=true;
}
function circleup(e){
drag3=false;
}
function circlemove(e){
if (drag3){
context.putImageData(img,0,0);
endx=e.x;
endy=e.y;
context.beginPath();
context.arc(Math.abs(endx+startx)/2,Math.abs(endy+starty)/2,Math.sqrt(Math.pow(endx-startx,2)+Math.pow(endy-starty,2))/2, 0, Math.PI*2, true); 
context.closePath();
context.stroke();
if (f==1){
context.fill();
}
}}}

//Color select tool

function color(colour){
context.strokeStyle=colour;
context.fillStyle=colour;
}

//Eraser tool


function erase(){
canvas.onmousedown=erasedown;
canvas.onmouseup=eraseup;
canvas.onmousemove=erasemove;
function erasedown(e){
  drag5=true;
}
function eraseup(e){
drag5=false;
}
function erasemove(e){
if(drag5){
x=e.x;
y=e.y;
context.clearRect(x,y,20,20);
}}}


//Brush tool

function brush(){
canvas.onmousedown=brushdown;
canvas.onmouseup=brushup;
canvas.onmousemove=brushmove;
function brushdown(e){
drag9=true;
}
function brushup(e){
drag9=false;
}
function brushmove(e){
if (drag9){
 x=e.x;
 y=e.y;
 context.beginPath();
 for(var i=0;i<7;i=i+0.1){
context.arc(x+i,y+i,2, 0, Math.PI*2, true);
}
context.closePath();
context.fill();
}}}


//Spray tool

function spray(){
canvas.onmousedown=spraydown;
canvas.onmouseup=sprayup;
canvas.onmousemove=spraymove;
function spraydown(e){
x=e.x;
y=e.y;
 drag8=true;
for (var i=0;i<20;i=i+6){
context.fillRect(x+i,y+i,1.5,1.5);
context.fillRect(x-i,y-i,1.5,1.5);
context.fillRect(x-i,y+i,1.5,1.5);
context.fillRect(x+i,y-i,1.5,1.5);
context.fillRect(x-i,y,1,1);
context.fillRect(x,y-i,1,1);
context.fillRect(x,y+i,1,1);
context.fillRect(x+i,y,1,1);
}
context.beginPath();
for(var i=0;i<12;i=i+4){
context.arc(x+i,y+i,1.3, 0, Math.PI*2, true); 
context.arc(x-i,y+i,1.3, 0, Math.PI*2, true);
context.arc(x+i,y-i,1.3, 0, Math.PI*2, true);
context.arc(x-i,y-i,1.3, 0, Math.PI*2, true); 
context.arc(x,y-i,1.3, 0, Math.PI*2, true);
context.arc(x-i,y,1.3, 0, Math.PI*2, true);
context.arc(x,y+i,1.3, 0, Math.PI*2, true);
context.arc(x+i,y,1.3, 0, Math.PI*2, true);
} 
context.closePath();
context.fill();
}
function sprayup(e){
drag8=false;
}
function spraymove(e){
if (drag8){
x=e.x;
y=e.y;
for (var i=0;i<20;i=i+6){
context.fillRect(x+i,y+i,1,1);
context.fillRect(x-i,y-i,1,1);
context.fillRect(x+i,y-i,1,1);
context.fillRect(x-i,y+i,1,1);
context.fillRect(x-i,y,1,1);
context.fillRect(x,y-i,1,1);
context.fillRect(x,y+i,1,1);
context.fillRect(x+i,y,1,1);
}
context.beginPath();
for(var i=0;i<12;i=i+4){
context.arc(x+i,y+i,1.3, 0, Math.PI*2, true); 
context.arc(x-i,y+i,1.3, 0, Math.PI*2, true);
context.arc(x+i,y-i,1.3, 0, Math.PI*2, true);
context.arc(x-i,y-i,1.3, 0, Math.PI*2, true);   
context.arc(x,y-i,1.3, 0, Math.PI*2, true);
context.arc(x-i,y,1.3, 0, Math.PI*2, true);
context.arc(x,y+i,1.3, 0, Math.PI*2, true);
context.arc(x+i,y,1.3, 0, Math.PI*2, true);
}
context.closePath();
context.fill();

}}
}
</script>
</html>


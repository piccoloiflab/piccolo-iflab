const LARGO_X = 640;
const ALTO_Y = 640;
var drag1;
var canvas = document.getElementById("myCanvas");
var context=canvas.getContext('2d');
var texto = "start\n";
context.lineWidth=1;

//Line tool
function line(){
    canvas.onmousedown=linedown;
    canvas.onmouseup=lineup;		//Pintar 
    canvas.onmousemove=linemove;
    function linedown(e){
	img=context.getImageData(0,0,canvas.width,canvas.height);
	startx=e.x;
	starty=e.y;
	drag1=true;
	texto = texto + "line\n";
	texto = texto + porcentaje(startx) + "\n" + porcentaje(starty) + "\n";
    }
    function lineup(e){
	texto = texto + porcentaje(endx) + "\n" + porcentaje(endy) + "\n";
	drag1=false;
	document.getElementById ('programa').value = texto;
    }
    function linemove(e){
	if (drag1){
	context.putImageData(img,0,0);
	endx=e.x;
	endy=e.y;
	context.beginPath();
	context.moveTo(startx,starty);
	context.lineTo(endx,endy);
	context.stroke();
	context.closePath();
	}
    }
}

//Line tool
function pencil(){
    canvas.onmousedown=pencildown;
    canvas.onmouseup=pencilup;		//Pintar 
    canvas.onmousemove=pencilmove;
    function pencildown(e){
		x=e.x;
		y=e.y;
		draw=true;
		texto = texto + "pencil\n";
		texto = texto + porcentaje(x) + "\n" + porcentaje(y) + "\n";
    }
    function pencilup(e){
		draw=false;
		texto = texto +"stop\n";
   		document.getElementById ('programa').value = texto;
    }
    function pencilmove(e){
		if (draw){
			a =event.x;
   			b =event.y;
   			context.beginPath();
   			context.moveTo(x,y);
			context.lineTo(a,b);
			texto = texto + porcentaje(a) + "\n" + porcentaje(b) + "\n";
   			document.getElementById ('programa').value = texto;
   			context.stroke();
			context.closePath();
   			x=a;
   			y=b;
		}
    }
}

//Rectangle tool

function rectangle(){
	canvas.onmousedown=rectdown;
	canvas.onmouseup=rectup;
	canvas.onmousemove=rectmove;
	function rectdown(e){
 		img=context.getImageData(0,0,canvas.width,canvas.height);
 		startx=e.x;
 		starty=e.y;
 		drag2=true;
 		texto = texto + "rectangle\n";
		texto = texto + porcentaje(startx) + "\n" + porcentaje(starty) + "\n";
	}
	function rectup(e){
		drag2=false;
		if(rectw < 0){
			rectw = LARGO_X + rectw;
		}
		if(recth < 0){
			recth = ALTO_Y + recth;
		}
		texto = texto + porcentaje(rectw) + "\n" + porcentaje(recth) + "\n";
		document.getElementById ('programa').value = texto;
	}
	function rectmove(e){
		if (drag2){
			context.putImageData(img,0,0);
			rectw=e.x-startx;
			recth=e.y-starty;
			context.strokeRect(startx,starty,rectw,recth);
			if (f==1){
				context.fillRect(startx,starty,rectw,recth);
			}
		}
	}
}

//Circle tool

function circle(){
	canvas.onmousedown=circledown;
	canvas.onmouseup=circleup;
	canvas.onmousemove=circlemove;
	function circledown(e){
		img=context.getImageData(0,0,canvas.width,canvas.height);
		startx=e.x;
		starty=e.y;
		drag3=true;
	}
	function circleup(e){
		drag3=false;
		texto = texto + "circle\n";
		texto = texto + porcentaje(circule_x) + "\n" + porcentaje(circule_y) + "\n" + porcentajeInv(circule_r) + "\n";
		document.getElementById ('programa').value = texto;
	}
	function circlemove(e){
		if (drag3){
			context.putImageData(img,0,0);
			endx=e.x;
			endy=e.y;
			context.beginPath();
			circule_x = Math.abs(endx+startx)/2;
			circule_y = Math.abs(endy+starty)/2;
			circule_r = Math.sqrt(Math.pow(endx-startx,2)+Math.pow(endy-starty,2))/2;
			context.arc(circule_x, circule_y, circule_r, 0, Math.PI*2, true); 
			context.closePath();
			context.stroke();
			if (f==1){
				context.fill();
			}
		}
	}
}

function porcentajeInv(dato){
    return parseInt(dato*100/LARGO_X);
}
function porcentaje(dato){
    return (-1*parseInt(dato*100/LARGO_X)+100);
}

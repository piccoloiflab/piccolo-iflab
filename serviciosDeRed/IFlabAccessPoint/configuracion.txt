Los directorios que no existan deben ser creados.

1. Copiar el archivo dhcpd.conf dentro del directorio /etc/dhcp

2. Copiar el archivo hostapd.conf dentro del directorio /etc/hostapd

3. Copiar el archivo isc-dhcp-server dentro del directorio /etc/default

4. copiar el archivo S51hostapd dentro del directorio /etc/init.d y dar permisos
	de ejecución.

Nota: Antes de ésto, es evidente que para la llave wifi RTL8192CU se debió cross compilar
el hostapd y que éste es instalado en /usr/sbin de la tarjeta IfLab. exite el ejecutable 
hostapd en el directorio servicios\ de\ red/ listo para ser copiado en el directorio /usr/sbin

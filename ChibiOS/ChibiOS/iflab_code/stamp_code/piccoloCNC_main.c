#include "ch.h"
#include "hal.h"
#include "test.h"
#include "atmel_pwm.h"
#include "atmel_pio.h"
#include "cnc/CNC.c"

#define LEVANTAR 30
#define PUNTO 0

/*
 * util
 * 0 < util < resolucion
 * 0 < util < 5000
 * frecuencia=muestreo/resolucion=1000000/10000
 * frecuencia=200Hz
 * periodo=1/frecuencia
 * periodo=5 ms
 * Estos datos hacen referencia a la frecuencia obtenida para el PWM
 * el ciclo util 
 * */
 
 /*
  * PA0 = y
  * PA1 = x
  * PA2 = z 
  * */
  
   /*
  * PA0 = x
  * PA1 = y
  * PA2 = z 
  * */
  
  /*
   * La uart 1 (&SD1)
   * RX = PA9
   * TX = PA10
   * */

#define MUESTREO   1000000	//muestreo
#define RESOLUCION 5000 	//Milisegundos resolucion

static const Pin pins[] = {
    PIN_PWM_CH0,
    PIN_PWM_CH1,
    PIN_PWM_CH2,
};

int velocidad1 = 100;
int velocidad = 50;
int dataIn[4];

int main (void){
    
    halInit();
    chSysInit();
    sdStart(&SD1, NULL);
    sdStart(&SD2, NULL);  
    
    PIO_Configure(pins, PIO_LISTSIZE(pins));

    //Configurando GPIO
    palSetPadMode(IOPORT1, PIOA_ENABLE_PWM, PAL_MODE_OUTPUT_PUSHPULL);

    // Configuración e inicialización del PWM

    PWM_Initialize(); //Eneable PWMC peripheral clock
    PWMC_ConfigureClocks(MUESTREO, 0, 48000000);
    

    // Configurar los 3 canales

    PWMC_ConfigureChannel(0, PWM_CMR_CPRE_CLKA, 0, 0);
    PWMC_ConfigureChannel(1, PWM_CMR_CPRE_CLKA, 0, 0);
    PWMC_ConfigureChannel(2, PWM_CMR_CPRE_CLKA, 0, 0);
    
    // Establecer el periodo de los canales

    PWMC_SetPeriod(0, RESOLUCION);
    PWMC_SetPeriod(1, RESOLUCION);
    PWMC_SetPeriod(2, RESOLUCION);
    
    // Habilitar los canales
    
    PWMC_EnableChannel(0);
    PWMC_EnableChannel(1);
    PWMC_EnableChannel(2);
    
    //while(TRUE){
	//dataIn[0] = sdGet(&SD2);
	//sdPut(&SD2, dataIn[0]);
	//sdIncomingDataI(&SD2, dataIn[0]);
	//chprintf((BaseChannel *)&SD2,"%c",dataIn[0]);
    //}
 
    while(TRUE){
	chprintf((BaseChannel *)&SD1,"comunicacion SAM3N \r\n");
	palSetPad(IOPORT1, PIOA_ENABLE_PWM);
	dataIn[0] = sdGet(&SD2);
	switch(dataIn[0]){
	    case 'l':	//--PrintLine
		for(int i = 0; i < 4; i++){
		    dataIn[i] = sdGet(&SD2);
		}
		chprintf((BaseChannel *)&SD1,"Pintando Linea con:\r\n");
		chprintf((BaseChannel *)&SD1,"a(%d, %d) b(%d, %d) \r\n",dataIn[0], dataIn[1], dataIn[2], dataIn[3]);
		palClearPad(IOPORT1, PIOA_ENABLE_PWM);
		printLine(dataIn[0], dataIn[1], dataIn[2], dataIn[3], velocidad);
		palSetPad(IOPORT1, PIOA_ENABLE_PWM);
	    break;
	    case 'r':	//--PrintRectangle
		for(int i = 0; i < 4; i++){
		dataIn[i] = sdGet(&SD2);
		}
		chprintf((BaseChannel *)&SD1,"Pintando Rectangulo con:\r\n");
		chprintf((BaseChannel *)&SD1,"a(%d, %d) b(%d, %d) \r\n",dataIn[0], dataIn[1], dataIn[2], dataIn[3]);
		palClearPad(IOPORT1, PIOA_ENABLE_PWM);
		printRect(dataIn[0], dataIn[1], dataIn[2], dataIn[3], velocidad);
		palSetPad(IOPORT1, PIOA_ENABLE_PWM);
	    break;
	    case 'c':	//--PrintCircle
		for(int i = 0; i < 3; i++){
		dataIn[i] = sdGet(&SD2);
		}
		chprintf((BaseChannel *)&SD1,"Pintando Circulo con:\r\n");
		chprintf((BaseChannel *)&SD1,"centro(%d, %d) r=%d \r\n",dataIn[0], dataIn[1], dataIn[2]);
		palClearPad(IOPORT1, PIOA_ENABLE_PWM);
		printCircle(dataIn[0], dataIn[1], dataIn[2], velocidad);
		palSetPad(IOPORT1, PIOA_ENABLE_PWM);
	    break;
	    case 'i':	//--PrintImagen
		chprintf((BaseChannel *)&SD1,"Imagen:\r\n");
		palClearPad(IOPORT1, PIOA_ENABLE_PWM);
		dataIn[0] = sdGet(&SD2);
		sdPut(&SD2, dataIn[0]);
		dataIn[1] = sdGet(&SD2);
		sdPut(&SD2, dataIn[1]);		
		while(dataIn[0] != 'e'){
		    chprintf((BaseChannel *)&SD1,"%d, %d \r\n", dataIn[0], dataIn[1]);
		    pintar(dataIn[0], dataIn[1], LEVANTAR, velocidad1);
		    pintar(dataIn[0], dataIn[1], PUNTO, velocidad);
		    dataIn[0] = sdGet(&SD2);
		    sdPut(&SD2, dataIn[0]);
		    dataIn[1] = sdGet(&SD2);
		    sdPut(&SD2, dataIn[1]);		    
		}
		chprintf((BaseChannel *)&SD1,"Saliendo de imagen:\r\n");
		palSetPad(IOPORT1, PIOA_ENABLE_PWM);
	    break;
	    case 'p':	//--PrintImagen
		chprintf((BaseChannel *)&SD1,"Pencil:\r\n");
		palClearPad(IOPORT1, PIOA_ENABLE_PWM);
		    dataIn[0] = sdGet(&SD2);
		    sdPut(&SD2, dataIn[0]);
		    dataIn[1] = sdGet(&SD2);
		    sdPut(&SD2, dataIn[1]);
		pintar(dataIn[0], dataIn[1], LEVANTAR, 500);
		while(dataIn[0] != 'e'){
		    chprintf((BaseChannel *)&SD1,"%d, %d \r\n", dataIn[0], dataIn[1]);
		    pintar(dataIn[0], dataIn[1], PUNTO, velocidad);
		    dataIn[0] = sdGet(&SD2);
		    sdPut(&SD2, dataIn[0]);
		    dataIn[1] = sdGet(&SD2);
		    sdPut(&SD2, dataIn[1]);
		}
		pintar(0, 0, LEVANTAR, 250);
		chprintf((BaseChannel *)&SD1,"Saliendo de pencil:\r\n");
		palSetPad(IOPORT1, PIOA_ENABLE_PWM);
	    break;
	    
	}
    }
}

#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"
#include "atmel_twi.h"
#include "atmel_twid.h"
#include "atmel_pio.h"
#include "rtc_ds3231.h"


#include "async.h"

#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>

CH_IRQ_HANDLER(TWI0_IRQHandler)
{
    CH_IRQ_PROLOGUE();
    TWI0_IrqHandler();
    CH_IRQ_EPILOGUE();
} 
/*----------------------------------------------------------------------------
 *        Global functions
 *----------------------------------------------------------------------------*/

/**
 * \brief Application entry point for TWI eeprom example.
 *
 * \return Unused (ANSI-C compatibility).
 */

extern int main( void )
{
	halInit();
	chSysInit();
	sdStart(&SD2, NULL);  
    
    unsigned char hrs , min , seg ;
    short _am_pm ;
    
	chThdSleepMilliseconds(1500);
	chprintf((BaseChannel *)&SD2,"Starting Ds3231 test\n\r");
    DS3231_init();
    setTime(23, 57, 0 , 0 , _24_hour_format);
	while (1){
	chprintf((BaseChannel *)&SD2,"[Ctrl: %x StaCrl : %x]\n\r", DS3231_Read(controlREG) , DS3231_Read(staCrlREG));
	getTime(&hrs, &min, &seg , &_am_pm , _24_hour_format );
	chThdSleepMilliseconds(500);
	chprintf((BaseChannel *)&SD2,"[ %d : %d : %d ]", hrs , min , seg);
	}
	
}

#include "ch.h"
#include "hal.h"
#include "test.h"
#include "atmel_pwm.h"
#include "atmel_pio.h"

#define PWM_FREQUENCY   1000000 //muestreo
//#define MAX_DUTY_CYCLE  100
//#define MIN_DUTY_CYCLE  0
#define periodo 10000 //Milisegundos resolucion
//#define util 700 //

/*
 * resolucion = periodo
 * util
 * mustreo=resolucion*frecuencia
 * 0 < util < resolucion
 * */

static const Pin pins[] = {
    PIN_PWM_CH0,
    PIN_PWM_CH1,
    PIN_PWM_CH2

};

//Función principal

int main (void){
	int x, y; 
	int z[135];
	uint16_t NRows;
	uint16_t NColu;

    halInit();
    chSysInit();

    PIO_Configure(pins, PIO_LISTSIZE(pins));

    // Configuración e inicialización del PWM

    PWM_Initialize(); //Eneable PWMC peripheral clock
    PWMC_ConfigureClocks(PWM_FREQUENCY, 0, 48000000);
    

    // Configurar los 3 canales

    PWMC_ConfigureChannel(0, PWM_CMR_CPRE_CLKA, 0, 0);
    PWMC_ConfigureChannel(1, PWM_CMR_CPRE_CLKA, 0, 0);
    PWMC_ConfigureChannel(2, PWM_CMR_CPRE_CLKA, 0, 0);
    
	// Establecer el periodo de los canales

    PWMC_SetPeriod(0, periodo);
    PWMC_SetPeriod(1, periodo);
	PWMC_SetPeriod(2, periodo);
    
    // Habilitar los canales
    
    PWMC_EnableChannel(0);
    PWMC_EnableChannel(1);
    PWMC_EnableChannel(2);
    
    // Ciclo de movimiento de los servomotores
    //z = 0;
    int j = 0;
    for (int i = 0; i <= 135; i++ ){
    z[i]=0;
	}
    for (int i = 0; i <= 135; i++ ){
    if(z [i] == 0){
		int j = i + 1;
		z [j] = 1;

				} 
    
}
    while(TRUE){ 
		int j = 0;
		/*while(TRUE){
			PWMC_SetDutyCycle(0, 8000);
			PWMC_SetDutyCycle(1, 8000);
			PWMC_SetDutyCycle(2, 8000);
			}*/
		//for( int j = 0; j <= 7; j++ ){
			//z[j] = ; Recibir dato por UART, filas y columnas de la imágen
			
			//}
		PWMC_SetDutyCycle(2, 8900);	
		for ( y = 9350; y >= 8000; y = y - 10 ){
		//Mover Eje y
		PWMC_SetDutyCycle(0, y);
			for( x = 9350; x >= 8000; x = x - 10 ){
					for( int i = 0; i <= 800; i++ ){
						//Mover Eje x
						PWMC_SetDutyCycle(1, x);
					}
					
						 //Recibir dato por UART, pixeles con color				
											
						if(z[j] == 0 && j <= 135){
							//Mover Eje z	
							for( int i = 0; i <= 70000; i++ ){
								PWMC_SetDutyCycle(2, 8000);
								}
							for( int i = 0; i <= 70000; i++ ){
								PWMC_SetDutyCycle(2, 8100);
								}
				
						}
					j = j + 1;	
					
						
				}
			for( int i = 0; i <= 100000; i++ ){	
				PWMC_SetDutyCycle(2, 8900);	
				}
			//for(int i = 0; i <= 600; i++ ){
				//} 	
			}   
    }
    return 0;
}


/*TODO 
*1. Recibir datos por UART
*2. Cambiar condición para salir del programa
*3. cambiar condiciones de salto de línea y columna.
* IMPORTANTE : Resolución de las imágenes de 135 x 135 puntos */

#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"
#include "atmel_pwm.h"
#include "atmel_pio.h"

#define PWM_FREQUENCY               1000
#define MAX_DUTY_CYCLE              100
#define MIN_DUTY_CYCLE              0



int16_t adc_values1[1] = { 0 } ;
int16_t adc_values2[1] = { 0 } ;

static const Pin pins[] = {
    PIN_PWM_CH0,
    PIN_PWM_CH1,
    PIN_PWM_CH2
};

/*
 * Application entry point.
 */
int main(void) {
	

	int DUTY_CYCLE;
	int status ;

	halInit();
	chSysInit();
	
	PIO_Configure(pins, PIO_LISTSIZE(pins));

	sdStart(&SD2, NULL);  /* Activates the serial driver 2 */

	/*PWM configuration*/
	
	PWM_Initialize(); /* Enable PWMC peripheral clock */
	PWMC_ConfigureClocks(PWM_FREQUENCY, 0 , 48000000 );
	
	/* Configure Channel 0*/ 
	PWMC_ConfigureChannel(0, PWM_CMR_CPRE_CLKA, 0, 0);
  	PWMC_SetPeriod(0, MAX_DUTY_CYCLE);
  	PWMC_SetDutyCycle(0, MIN_DUTY_CYCLE);
  	PWMC_EnableChannel(0);
  
  	/* Configure PWMC channel 1 
  	PWMC_ConfigureChannel(1, PWM_CMR_CPRE_CLKA, 0, 0);
  	PWMC_SetPeriod(1, MAX_DUTY_CYCLE);
  	PWMC_SetDutyCycle(1, MIN_DUTY_CYCLE);
  	PWMC_EnableChannel(1);*/

	return(0);
}

#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"

int main (void){
    uint8_t b;
    halInit();
    chSysInit();
    sdStart(&SD1, NULL);  
    sdStart(&SD2, NULL);
    while(TRUE){
	sdIncomingDataI(&SD1, b);
	chprintf((BaseChannel *)&SD1,"%d", b);
    }
    return 0;
}

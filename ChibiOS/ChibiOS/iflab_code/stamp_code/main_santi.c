#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"
#include "atmel_adc.h"
#include "atmel_pwm.h"
#include "atmel_pio.h"
#include "atmel_pio_it.h"
#include "atmel_usart.h"


const Pin pinPA6 = PIN_ROT0 ;
unsigned int count;

int i= 0 ;
char read_string ;

static WORKING_AREA(waThread2, 128);
static msg_t Thread2(void *arg) { 
  (void)arg;
  while(TRUE){
    msg_t c;
    c = chIOGet(&SD1);
    
    if(c == 0x69){
        count = 0;
    }
    
    if (c == 0x72){
        chprintf((BaseChannel *)&SD1,"Contador : %d \n\r", count);
    }

  }
  return(0);
}

static WORKING_AREA(waThread1, 128);
static msg_t Thread1(void *arg) {
  (void)arg;
  while (TRUE) {
    //chprintf((BaseChannel *)&SD2,"Contador : %d \n\r", count);
    chThdSleepMilliseconds(500);
  }
  return(0);
}

static void _Button1_Handler( const Pin* pin )
{
    count++;
    //chprintf((BaseChannel *)&SD2,"Contador : %d \n\r", count);
}

CH_IRQ_HANDLER(PIOA_IRQHandler)
{
    CH_IRQ_PROLOGUE();
    PioInterruptHandler( ID_PIOA, PIOA ) ;
    CH_IRQ_EPILOGUE();
}

static void _ConfigurePin(void){
    PIO_Configure( &pinPA6, 1 ) ;
    
    /* Adjust pio debounce filter patameters, uses 10 Hz filter. */
    //PIO_SetDebounceFilter( &pinPA6, 10 ) ;

    /* Initialize pios interrupt handlers, see PIO definition in board.h. */
    PIO_ConfigureIt( &pinPA6, _Button1_Handler ) ; /* Interrupt on rising edge  */

    /* Enable PIO controller IRQs. */
    nvicEnableVector(PIOA_IRQn , CORTEX_PRIORITY_MASK(SAM3_USART0_PRIORITY));

    /* Enable PIO line interrupts. */
    PIO_EnableIt( &pinPA6 ) ;


}

void main (void)
 {
  
    halInit();
	chSysInit();
	sdStart(&SD1, NULL);  
	count = 0;
	chprintf((BaseChannel *)&SD1,"Prueba Santi\n\r");
	PIO_InitializeInterrupts();
    _ConfigurePin() ;
  //  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);
    chThdCreateStatic(waThread2, sizeof(waThread2), NORMALPRIO, Thread2, NULL);
	
 }

#include "ch.h"
#include "hal.h"
#include "test.h"
#include "atmel_pwm.h"
#include "atmel_pio.h"
#include <math.h>

#define PI 3.14159265
#define PASO_CIRCULO 0.0174532925
#define	CHPRINTF_USE_FLOAT   FALSE
#define SUBIR 100
#define BAJAR 0

//--Arreglo para los tres servomotores
struct servoPWM{
    float x;
    float y;
};

//--Vectores bidimencionales
struct vector2D{
    float x;
    float y;
};

//--Pintara cualquier forma de PWM y se encarga de hacerlo a
// alguna velocidad asignada
void pintar(int x, int y, int z, int velocity);

//-- Funcion que se encargara de hacer los recorridos con
// un unico parametro t, tambien se le pude asignar la velocidad
void printLine(int x1, int y1, int x2, int y2, int velocity);

void printRect(int x1, int y1, int x2, int y2, int velocity);

void printCircle(int x1, int y1, int r, int velocity);

void printLine(int x1, int y1, int x2, int y2, int velocity){
    
    struct servoPWM pwm;
    struct vector2D a={x1, y1};
    struct vector2D b={x2, y2};
    
    // Suponiendo 80, z en posición para escribir
    for (float t = 0; t <= 1 ; t = t + 0.01){
	pwm.x = a.x + t*(b.x - a.x);
	pwm.y = a.y + t*(b.y - a.y);
	//chprintf((BaseChannel *)&SD1,"%d, %d, %d\r\n", (int)pwm.x, (int)pwm.y, (int)(100*t));
	if((int)(100*t) == 0){
	    pintar(pwm.x, pwm.y, SUBIR, 500);
	    //chprintf((BaseChannel *)&SD1,"Aqui:\r\n");
	}
	pintar(pwm.x, pwm.y, BAJAR, velocity);
    }
    pintar(pwm.x, pwm.y, SUBIR, velocity);
}

void printRect(int x1, int y1, int x2, int y2, int velocity){
    struct servoPWM pwm;
    struct vector2D a={x1, y1};
    struct vector2D b={x2, y2};
    pintar(a.x, a.y, SUBIR, 500); 
    for (float t = 0; t <= 1 ; t = t + 0.01){
	pwm.x = a.x + t*(b.x - a.x);
	pintar(pwm.x, a.y, BAJAR, velocity);
    }
    for (float t = 0; t <= 1 ; t = t + 0.01){
	pwm.y = a.y + t*(b.y - a.y);
	pintar(b.x, pwm.y, BAJAR, velocity);
    }
    for (float t = 0; t <= 1 ; t = t + 0.01){
	pwm.x = b.x + t*(a.x - b.x);
	pintar(pwm.x, b.y, BAJAR, velocity);
    }
    for (float t = 0; t <= 1 ; t = t + 0.01){
	pwm.y = b.y + t*(a.y - b.y);
	pintar(a.x, pwm.y, BAJAR, velocity);
    }
    pintar(pwm.x, pwm.y, SUBIR, velocity);
}

void printCircle(int x, int y, int r, int velocity){
    struct servoPWM pwm;
    struct vector2D a={x, y};
    pintar(r + a.x, a.y, SUBIR, 500);
    for(double t = 0; t <= 2*PI; t = t + PASO_CIRCULO){
	pwm.x = r*cos(t) + a.x;
	pwm.y = r*sin(t) + a.y;
	pintar(pwm.x, pwm.y, BAJAR, velocity);
    }
    pintar(pwm.x, pwm.y, SUBIR, velocity);
}

void pintar(int x, int y, int z, int velocity){
    /* z = 0 baja lápiz
     * z = 100 sube lápiz
     * */
    x = 10*x + 3000;
    y = 10*y + 3000;
    z = 10*z + 3000;
    PWMC_SetDutyCycle(0, x);
    PWMC_SetDutyCycle(1, y);
    PWMC_SetDutyCycle(2, z);
    chThdSleepMilliseconds(velocity);
}
